<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });

Auth::routes();

Route::get('/', 'FrontController@index');
Route::get('/index/cari','FrontController@cari');
Route::get('/single/{id}', 'SingleController@single');
Route::get('/buka_lowongan', 'BukaLowonganController@bukalow')->name('buka_lowongan');
Route::post('/buka_lowongan/proses', 'BukaLowonganController@proses_upload');


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/add-kategori', 'HomeController@kategori');
Route::get('aktif/{id}','HomeController@aktif');
Route::get('nonaktif/{id}','HomeController@nonaktif');
Route::post('proses/add-kategori', 'HomeController@addkategori');
Route::post('proses/update-kategori', 'HomeController@updatekategori');
