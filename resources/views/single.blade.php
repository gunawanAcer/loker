<?php

    function rupiah($angka){

        $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
        return $hasil_rupiah;
     
    }

?>

@include('layouts.header')

@foreach($single as $g)
    <div class="ban-fot">
      <div class="wrap">
        <div class="tikat-s">{{$g->judul_lowongan}}</div>
        <p>Posting Pada : {{$g->created_at}}</p>
          <div class="row">
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-2">
                        <button type="button" class="btn btn-success">{{$g->jenis}}</button>
                    </div>
                    <div class="col-md-5">
                        <div style="font-family: raleway; color: #191970; margin-top: 10px; font-size: 16px;"><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;&nbsp;{{$g->alamat}}</div>
                    </div>
                </div>
                <div class="ban-fot-s">
                    <div class="row">
                        <div class="col-md-1">
                            <img src="{{ url('/data_image/'.$g->image) }}" width="100%" style="margin-top: 5px;">
                        </div>
                        <div class="col-md-7">
                            <div class="tit-s"> {{$g->nama_perusahaan}}</div> 
                            <p style="font-style: italic; color: #cecece;">Lowongan Kerja Ubud</p>
                        </div>
                        <div class="col-md-4"><h3><?php echo rupiah($g->gaji); ?></h3></div>
                    </div>
                </div>

                <p>
                    <?php echo htmlspecialchars_decode($g->deskripsi); ?></p>
                </p>
                <!-- <button type="button" class="btn btn-danger"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;Lihat Kontak</button> -->

                <div class="panel panel-primary" style="border-radius: 0; margin-top: 50px;">
                    <div class="panel-heading" style="border-radius: 0";>
                        <h3 class="panel-title">Informasi Kontak</h3>
                    </div>
                    <div class="panel-body">
                        <label>Email</label>
                        <p class="inf"><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;&nbsp;{{$g->email}}</p>
                        <label>No Telp</label>
                        <p class="inf"><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;&nbsp;{{$g->no_tlp}}</p>
                        <label>Alamat</label>
                        <p class="inf"><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;&nbsp;{{$g->alamat}}</p>
                    </div>
                </div>

            </div>

            @include('layouts.sidebar')

          </div>
      </div>
    </div>
@endforeach

@include('layouts.footer')

