<?php
function rupiah($angka)
{

    $hasil_rupiah = "Rp " . number_format($angka, 2, ',', '.');
    return $hasil_rupiah;
}
?>

@include('layouts.header')

<div class="ban-fot">
    <div class="wrap">
        <div class="tikat"><i class="fa fa-search" aria-hidden="true"></i>&nbsp;Info Lowongan Kerja</div>
        <div class="row">
            <div class="col-md-9">
                <div class="katagori">
                    <form action="/index/cari" method="GET">
                        <div class="row">
                            <div class="col-md-12">
                                <select class="form-control" name="cat" required="">
                                    @foreach ($kategori as $data)
                                    <option value="{{ $data->id }}">{{ $data->nama_kategori}} ({{ $data->count }})</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-12"><input type="submit" class="btn btn-success" style="width:100%;" value="Cari Pekerjaan"></div>
                        </div>
                    </form>
                </div>
                @foreach($lowongan as $g)
                <div class="katagori2">
                    <div class="row">
                        <div class="col-md-2"><img src="{{ url('/data_image/'.$g->image) }}" width="100%"></div>
                        <div class="col-md-10">
                            <div class="tikat3">{{$g->judul_lowongan}} </div>
                            <p class="l-p">{{$g->nama_perusahaan}}  <br><button type="button" class="btn btn-success">{{$g->jenis}}</button>
                            </p>
                            
                            <div class="data-l">
                                
                                <p><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;&nbsp;{{$g->alamat}}</p>
                                <p><i class="fa fa-inbox" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo rupiah($g->gaji); ?></p>
                            </div>
                        </div>
                        <div class="col-md-12">

                            <!-- <?php echo htmlspecialchars_decode(substr($g->deskripsi, 0, 600)); ?>.... -->
                            <hr />
                            <a href="/single/{{ $g->id }}"><button type="button" class="btn btn-info">Baca Selengkapnya</button></a>



                        </div>
                    </div>
                </div>
                @endforeach
            </div>

            @include('layouts.sidebar')

        </div>
        {{ $lowongan->links() }} <br />
        <!--           <a href=""><p style="margin-top: 15px;">Selanjutnya..</p></a> -->
    </div>
</div>

@include('layouts.footer')