<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Laravel Loker Ubud
                </div>

                <div class="links">
                    <a href="https://laravel.com/docs">Documentation</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div>
    </body>
</html>


<!-- @include('layouts.header');

    <div class="ban-fot">
      <div class="wrap">
        <div class="tikat"><i class="fa fa-search" aria-hidden="true"></i>&nbsp;Lowongan Kerja</div>
          <div class="row">
            <div class="col-md-9">
                <div class="katagori">
                  <div class="row">
                    <div class="col-md-6"><input type="text" class="form-control" name="" placeholder="Jenis Lowongan"></div>
                    <div class="col-md-6"><input type="text" class="form-control" name="" placeholder="Gaji Minimum"></div>
                    <div class="col-md-12"><button type="button" class="btn btn-default">Cari</button></div>
                  </div>
                </div>
                
                <div class="katagori2">
                    <div class="row">
                        <div class="col-md-2"><img src="{{ url('assets/img/contoh.jpg') }}" width="100%"></div>
                        <div class="col-md-10">
                            <div class="tikat3">Web Desinger & Programmer</div>
                            <p class="l-p">PT. Gunawan Corporation Group</p>
                            <div class="data-l">
                                <p><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;&nbsp;Taman Kelod Ubud</p>
                                <p><i class="fa fa-usd" aria-hidden="true"></i>&nbsp;&nbsp;Rp. 2.000.000,00</p>
                            </div>
                        </div>
                        <div class="col-md-12"> 
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vel turpis urna. Integer imperdiet libero nec enim pulvinar, vitae fermentum velit iaculis Donec malesuada commodo eros, ut auctor magna mattis sed. Nunc vel eros nulla. Nulla eget neque consectetur, ornare lectus nec, consequat risus....</p>
                            <button type="button" class="btn btn-info">Baca Selengkapnya</button>
                            <button type="button" class="btn btn-success">Kirim Lamaran</button>
                        </div>
                    </div>
                </div>

            </div>

            @include('layouts.sidebar');

          </div>
          <a href=""><p style="margin-top: 15px;">Selanjutnya..</p></a>
      </div>
    </div>

@include('layouts.footer') -->