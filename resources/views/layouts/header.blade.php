<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>Lowongan Kerja Ubud | Loker In Ubud</title>

  <!-- Bootstrap -->
  <link href="{{ url('assets/css/bootstrap.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ url('assets/css/style.css') }}" rel="stylesheet">

  <link rel="stylesheet" href="{{ url('assets/css/owl.carousel.css') }}">
  <link rel="stylesheet" href="{{ url('assets/css/owl.theme.css') }}">

  <script src="{{ asset('assets/sweetalert2/sweetalert2.min.js') }}"></script>
  <link href="{{ asset('assets/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="wrap">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="../"><h3 style="margin:8px; 0;">LOKER IN UBUD</h3></a>
      </div>
      <div id="navbar" class="navbar-collapse collapse">

        <ul class="nav-right">
          <a href="{{ route('buka_lowongan') }}"><button type="button" style="float:right;" class="btn btn-info">Buka Lowongan</button></a>

        </ul>
      </div>
    </div>
  </nav>


    <div class="barner">
      <img src="{{ url('assets/img/dream.png') }}" width="100%">
    </div>
