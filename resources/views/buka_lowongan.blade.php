@include('layouts.header')

	<div class="ban-fot">
      <div class="wrap">
        <div class="tikat-s">Buka Lowongan</div>
        <p>Tanggal : <?php echo  date("d-m-Y") ?></p>
          <div class="row">
            <div class="col-md-9">
            	<!-- @if(count($errors) > 0)
				<div class="alert alert-danger">
					@foreach ($errors->all() as $error)
					{{ $error }} <br/>
					@endforeach
				</div>
				@endif -->
				@if ($message = Session::get('Sukses'))
				<div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
					<strong>{{ $message }}</strong>
				</div>
				@endif
            	<div class="katagori2">
	            	<form action="/buka_lowongan/proses" method="POST" enctype="multipart/form-data">
	            		{{ csrf_field() }}

					  <div class="form-group">
					    <label>Judul Lowongan</label>
					    <input type="text" class="form-control" placeholder="Judul Lowongan" name="judul_lowongan" required="">
					  </div>
					  <div class="form-group">
					    <label>Nama Perusahaan</label>
					    <input type="text" class="form-control" placeholder="Nama Perusahaan" name="nama_perusahaan" required="">
					  </div>
					  <div class="form-group">
					    <label>Alamat</label>
					    <input type="text" class="form-control" placeholder="Alamat" name="alamat" required="">
					  </div>
					  <div class="form-group">
					    <label>No Tlp</label>
					    <input type="number" class="form-control" placeholder="No Tlp" name="no_tlp" required="">
					  </div>
					  <div class="form-group">
					    <label>Email</label>
					    <input type="text" class="form-control" placeholder="Email" name="email" required="">
					  </div>
					  <div class="form-group">
					    <label>Katagori Lowongan</label>
					    <select name="kategori" class="form-control" required="">
							@foreach ($kategori as $data)
							  <option value="{{ $data->id }}">{{ $data->nama_kategori}}</option>
							@endforeach
						</select>
					    <!-- <input type="text" class="form-control" placeholder="Kategori" name="kategori" required=""> -->
					  </div>
					  <div class="form-group">
					    <label>Jenis Lowongan</label>
					    <select name="jenis" class="form-control" required="">
							  <option value="Full Time">Full Time</option>
							  <option value="Part Time">Part Time</option>
							  <option value="Magang/DW">Magang/DW</option>
							  <option value="Freelancer">Freelancer</option>
						</select>
					    <!-- <input type="text" class="form-control" placeholder="Jenis Lowongan" name="jenis" required=""> -->
					  </div>
					  <div class="form-group">
					    <label>Masa Aktif</label>
					    <input type="date" class="form-control" placeholder="Masa Aktif" name="masa_aktif" required="">
					  </div>
					  <div class="form-group">
					    <label>Besan Gaji</label>
					    <input type="number" class="form-control" placeholder="Besaran Gaji" name="gaji" required="">
					  </div>
					  <div class="form-group">
					    <label>Deskripsi Lowongan</label>
					    <textarea id="deskripsi" class="form-control" name="deskripsi" rows="10" cols="50" required=""></textarea>
					  </div>
					  <div class="form-group">
					    <label>Pilih Gambar (Tidak Harus)</label>
					    <input type="file" name="image">
					    <p class="help-block">Example block-level help text here.</p>
					  </div>
					  <button type="submit" class="btn btn-default">Buka Lowongan</button>
					</form>
				</div>

            </div>

            @include('layouts.sidebar');

          </div>
      </div>
    </div>

@include('layouts.footer')
