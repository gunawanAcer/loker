@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default">
                <div class="panel-heading">Form Kategori</div>

                <div class="panel-body">
                    @if(isset($_GET['form']) && $_GET['form']=='ubah')
                    <form action="{{url('proses/update-kategori')}}" method="post">
                        <div class="form-group">
                        {!! csrf_field() !!}
                        <?php 
                            $users = \App\Kategori::find($_GET['id']);
                        ?>
                            <label for="email">Nama Kategori :</label>
                            <input type="text"  name="nama_kategori" class="form-control" value="{{$users->nama_kategori}}">
                            <input type="hidden" name="id" value="{{$users->id}}">
                        </div>
                        <button type="submit" class="btn btn-primary">Ubah</button>
                        <a href="{{url('add-kategori')}}" class="btn btn-default">FORM TAMBAH</a>
                    </form>
                    @else
                    <form action="{{url('proses/add-kategori')}}" method="post">
                        <div class="form-group">
                        {!! csrf_field() !!}
                            <label for="email">Nama Kategori :</label>
                            <input type="text"  name="nama_kategori" class="form-control">
                        </div>
                        <button type="submit" class="btn btn-default">Tambah</button>
                    </form>
                    @endif
                </div>
            </div>


            <div class="panel panel-default">
                <div class="panel-heading">List Kategori</div>

                <div class="panel-body">
                <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nama Kategori</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($kategori as $kat)
                            <tr>
                                <td>{{$kat->id}}</td>
                                <td>{{$kat->nama_kategori}}</td>
                                <td><a href="?form=ubah&id={{$kat->id}}" class="btn btn-sm btn-warning">Ubah</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<style type="text/css">
    .pagination li {
        float: left;
        list-style-type: none;
        margin: 5px;
    }
</style>