@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Judul</th>
                                <th>Kategori</th>
                                <th>Deskripsi</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($lowongan as $low)
                            <tr>
                                <td>{{$low->judul_lowongan}}</td>
                                <td>{{$low->kategori}}</td>
                                <td>{!!$low->deskripsi!!}</td>
                                @if($low->status=='0')
                                <td><a href="{{url('/aktif/'.$low->id)}}"><button class="btn btn-primary">Aktifkan</button></a></td>
                                @else
                                <td><a href="{{url('/nonaktif/'.$low->id)}}"><button class="btn btn-danger">Noaktifkan</button></a></td>
                                @endif
                            </tr>
                            @endforeach

                        </tbody>
                    </table>

                    <br />
                    Halaman : {{ $lowongan->currentPage() }} <br />
                    Jumlah Data : {{ $lowongan->total() }} <br />
                    Data Per Halaman : {{ $lowongan->perPage() }} <br />
                    {{ $lowongan->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<style type="text/css">
    .pagination li {
        float: left;
        list-style-type: none;
        margin: 5px;
    }
</style>