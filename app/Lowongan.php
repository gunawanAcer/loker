<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lowongan extends Model
{
    protected $table = "lowongan";
 
    protected $fillable = ['judul_lowongan','nama_perusahaan','alamat','no_tlp','email','kategori','jenis','masa_aktif','deskripsi','gaji','image','status'
	];

    public function kategori(){
       return $this->hasMany('App\kategori','kategori');
     }
}
