<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lowongan;
use App\Kategori;
use DB;

class FrontController extends Controller
{
    public function index()
    {
    	
    	$kategori = Kategori::select(DB::raw('count(kategori) as count'),'nama_kategori','kategori.id')->leftjoin('lowongan','kategori','kategori.id')->orderBy('nama_kategori','asc')->groupBy('kategori')->get();
    	$lowongan = Lowongan::where("status",1)->where('masa_aktif','>',date("Y-m-d"))->orderby("id","desc")->paginate(20);
        return view('index',['lowongan' => $lowongan],['kategori' => $kategori]);
    }

    public function cari(Request $request)
    {
    	$cari = $request->cari;
        $cat = $request->cat;

    	$kategori = Kategori::select(DB::raw('count(kategori) as count'),'nama_kategori','kategori.id')->leftjoin('lowongan','kategori','kategori.id')->orderBy('nama_kategori','asc')->groupBy('kategori')->get();
        $lowongan = DB::table('lowongan')
        ->where("kategori",$cat)
        ->where('masa_aktif','>',date("Y-m-d"))
        ->orderby("created_at",'asc')
		->paginate(20);

        // var_dump($lowongan);
 
    	
		return view('index',['lowongan' => $lowongan],['kategori' => $kategori]);
    }

}
