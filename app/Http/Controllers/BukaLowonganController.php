<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lowongan;
use App\Kategori;
use Session;

class BukaLowonganController extends Controller
{
    public function bukalow()
    {
    	$kategori = Kategori::orderBy('nama_kategori','asc')->get();
    	$lowongan = Lowongan::get();
        return view('buka_lowongan',['lowongan' => $lowongan],['kategori' => $kategori]);
    }

 
	public function proses_upload(Request $request){
		if(empty($request->file('image'))){
			$nama_file = 'default.jpg';
		}else{
		$this->validate($request, [
			'image' => 'image|image|mimes:jpeg,png,jpg|max:2048',
		]);
 
		// menyimpan data file yang diupload ke variabel $file
		$image = $request->file('image');
 
		$nama_file = time()."_".$image->getClientOriginalName();
 
      	        // isi dengan nama folder tempat kemana file diupload
		$tujuan_upload = 'data_image';
		$image->move($tujuan_upload,$nama_file);
		
		}
		Lowongan::create([
			'judul_lowongan' => $request->judul_lowongan,
			'nama_perusahaan' => $request->nama_perusahaan,
			'alamat' => $request->alamat,
			'no_tlp' => $request->no_tlp,
			'email' => $request->email,
			'kategori' => $request->kategori,
			'jenis' => $request->jenis,
			'masa_aktif' => $request->masa_aktif,
			'gaji' => $request->gaji,
			'deskripsi' => $request->deskripsi,
			'image' => $nama_file,
			'status'=>'1'

		]);
 
		Session::flash('Sukses','Buka Lowongan Sukses, Tunggu Verifikasi Admin !!');
		return redirect()->back();
	}
}
