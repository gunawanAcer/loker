<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lowongan;
use App\Kategori;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['lowongan'] = Lowongan::paginate(20);
        return view('home', $data);
    }


    public function kategori()
    {
        $data['kategori'] = Kategori::paginate(20);
        return view('admin.kategori',$data);
    }

    public function aktif($id)
    {
        $data = Lowongan::find($id);

        $data->status = '1';

        $data->save();
        if ($data) {
            return redirect()->back();
        }else{
            echo "gagal";
        }
    }

    public function nonaktif($id)
    {
        $data = Lowongan::find($id);

        $data->status = '0';

        $data->save();
        if ($data) {
            return redirect()->back();
        }else{
            echo "gagal";
        }
    }

    public function addkategori(Request $request){
        $data = new Kategori;

        $data->nama_kategori = $request->nama_kategori;

        $data->save();
        if ($data) {
            return redirect()->back();
        }else{
            echo "gagal";
        }
    }

    public function updatekategori(Request $request){
        $data = new Kategori;
        $data = Kategori::find($request->id);
        $data->nama_kategori = $request->nama_kategori;

        $data->save();
        if ($data) {
            return redirect('add-kategori');
        }else{
            echo "gagal";
        }
    }
}
