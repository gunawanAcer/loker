<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lowongan;
use DB;

class SingleController extends Controller
{
    public function single($id)
    {
        $single = DB::table('lowongan')->where('id',$id)->get();
        return view('single',['single' => $single]);
    }
}
