<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLowonganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lowongan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul_lowongan');
            $table->string('nama_perusahaan');
            $table->string('alamat');
            $table->string('no_tlp');
            $table->string('email');
            $table->string('kategori');
            $table->string('jenis');
            $table->date('masa_aktif');
            $table->longText('deskripsi');
            $table->string('gaji');
            $table->string('image');
            $table->char('status')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lowongan');
    }
}
